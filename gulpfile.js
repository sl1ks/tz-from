/* global require */
var gulp = require('gulp');
var browserSync = require('browser-sync').create();
var sass = require('gulp-sass');
var notify = require('gulp-notify');
var plumber = require('gulp-plumber');
var path = require('path');
var data = require('gulp-data');
var cleanCSS = require('gulp-clean-css');
var autoprefixer = require('gulp-autoprefixer');
var pug = require('gulp-pug');
var watch = require('gulp-watch');
var gulp_watch_pug = require('gulp-watch-pug');
var less = require('gulp-less');

/* Change your directory and settings here */
var settings = {
    publicDir: './',
    sassDir: './scss',
    cssDir: './',
    /* change to disable system notification */
    systemNotify: true,
    proxy: ''
}


gulp.task('serve', ['sass'], function () {
    /**
     * watch for changes in sass files
     */
    gulp.watch("./sass/**/*.scss", ['sass']);
});

/**
 * sass task, will compile the .SCSS files,
 * and handle the error through plumber and notify through system message.
 */
gulp.task('sass', function () {
    return gulp.src('./sass/**/**/**/*.scss')
        .pipe(plumber({
            errorHandler: settings.systemNotify ? notify.onError("Error: <%= error.messageOriginal %>") : function (err) {
                console.log(" ************** \n " + err.messageOriginal + "\n ************** ");
                this.emit('end');
            }
        }))
        .pipe(sass({outputStyle: 'expanded'})) /* compressed */
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))

        .pipe(gulp.dest('./'))
});


gulp.task('default', ['serve']);


