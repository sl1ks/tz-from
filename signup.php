<?php include('database.php') ?>
<!DOCTYPE html>
<html>
<head>
  <title>Signup</title>
  <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
  <div class="page">
    <header class="header">
    	<h2>Sign up</h2>
    </header>	
    <div class="wrapper">
      <form class="form" method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
      	<?php include('errors.php'); ?>
      	<div class="input-group">
      	  <label>Username</label>
      	  <input type="text" name="username" placeholder="Enter username ..." value="<?php echo $username; ?>" required>
      	</div>
      	<div class="input-group">
      	  <label>Email</label>
      	  <input type="email" name="email" placeholder="Enter email ..." value="<?php echo $email; ?>" required>
      	</div>
      	<div class="input-group">
      	  <label>Password</label>
      	  <input type="password" name="password_1" required>
      	</div>
      	<div class="input-group">
      	  <label>Confirm password</label>
      	  <input type="password" name="password_2" required>
      	</div>
      	<div class="input-group">
      	  <button type="submit" class="button" name="reg_user">Register</button>
      	</div>
      	<p>
      		Already a member? <a href="login.php">Log in</a>
      	</p>
      </form>
    </div>
  </div>
</body>
</html>