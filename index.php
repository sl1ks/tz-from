<!DOCTYPE html>
<html>
<head>
	<title>Home</title>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
  <div class="page">
    <header class="header">
    	<h2>Hello</h2>
    </header>
    <div class="wrapper">
      <a class="button button-login" href="login.php">Log in</a>
      <a class="button button-signup" href="signup.php">Sign up</a>
    </div>
  </div>
</body>
</html>