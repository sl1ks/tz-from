<?php 
  session_start(); 

  if (!isset($_SESSION['username'])) {
    $_SESSION['msg'] = "You must log in first";
    header('location: login.php');
  }
  if (isset($_GET['logout'])) {
    session_destroy();
    unset($_SESSION['username']);
    header("location: login.php");
  }
?>
<!DOCTYPE html>
<html>
<head>
  <title>Account</title>
  <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
  <div class="page">
    <header class="header">
      <h2>Your Account</h2>
    </header>
      <!-- logged in user information -->
      <?php  if (isset($_SESSION['username'])) : ?>
        <div class="account-info">
          <p>Welcome <strong><?php echo $_SESSION['username']; ?></strong></p>
        </div>      
        <a href="account.php?logout='1'" class="button">Logout</a>
      <?php endif ?>
  </div>  
</body>
</html>