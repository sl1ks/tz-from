<?php include('database.php') ?>
<!DOCTYPE html>
<html>
<head>
  <title>Login</title>
  <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
  <div class="page">
    <header class="header">
    	<h2>Login</h2>
    </header>
    <div class="wrapper">	   
      <form class="form" method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
      	<?php include('errors.php'); ?>
        <div class="input-group">
      		<label>Username</label>
      		<input type="text" name="username" placeholder="Enter username" required>
      	</div>
      	<div class="input-group">
      		<label>Password</label>
      		<input type="password" name="password" placeholder="Enter password" required>
      	</div>
      	<div class="input-group">
      		<button type="submit" class="button" name="login_user">Login</button>
      	</div>
      	<p>
      		Not yet a member? <a href="signup.php">Sign up</a>
      	</p>
      </form>
    </div>
  </div>
</body>
</html>